% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/cdr3-venn_diagram.R
\name{venn_diagram}
\alias{venn_diagram}
\title{Plot the number of shared clonotypes between different samples in a venn diagram}
\usage{
venn_diagram(dataFrameList, height = 5, width = 15,
  title = "Venn Diagram")
}
\arguments{
\item{height}{integer() Set the height of the diagram.}

\item{width}{integer() Set the width of the diagram.}

\item{title}{string() Set the title of the plot}
}
\description{
A maximum of 5 samples are allowed!
}
\examples{
venn_diagram(l.test)
}

