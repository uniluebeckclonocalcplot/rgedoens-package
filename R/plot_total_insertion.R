#' Plot total insertions
#'
#' Plot total insertions in samples as histogram.
#'
#' @param l A list of data.frames
#' @param title string() Set the title of the plot
#' @param width integer() Set the width of the diagram.
#' @param height integer() Set the height of the diagram.
#' @examples
#' plot_total_insertion(l.test, range = 0:20)
#' @export
#' @import ggplot2

plot_total_insertion <- function(l, title = "Total insertions", range = 0:20, font.size = 15, 
    angle.x.label = 0, x.label.vjust = 0) {
    if (is.null(names(l))) 
        stop("There are no names for the list elements in l!")
    
    for (n in names(l)) {
        if (sum(is.na(l[[n]][, "Total.insertions"])) > 0) {
            stop("Element ", n, " has NAs in the Total.Insertions column!")
        }
    }
    
    max.insertions <- max(sapply(l, function(DF) max(DF[, "Total.insertions"])))
    
    insertions.vec.l <- lapply(l, function(DF) {
        tmpy <- rep(0, max.insertions + 1)
        names(tmpy) <- as.character(0:max.insertions)
        
        tbl <- table(DF[, "Total.insertions"])
        tmpy[names(tbl)] <- as.vector(tbl)
        
        if (sum(as.character(range) %in% names(tmpy)) == length(range)) 
            tmpy <- tmpy[as.character(range)] else warning("range does not match!, insertions values:", names(tmpy))
        
        # auf 1 normieren
        tmpy <- tmpy/sum(tmpy)
        
        return(tmpy)
    })
    
    df <- NULL
    for (i in seq_along(insertions.vec.l)) {
        vec <- insertions.vec.l[[i]]
        df <- rbind(df, data.frame(source = names(l)[i], insertions = as.integer(names(vec)), 
            Percentage = vec))
    }
    
    ggplot(df, aes(x = insertions, y = Percentage, fill = source)) + geom_bar(position = "dodge", 
        stat = "identity") + ggtitle(title) + scale_x_continuous(breaks = range, 
        labels = range) + theme(text = element_text(size = font.size), axis.text.x = element_text(angle = angle.x.label, 
        vjust = x.label.vjust))
}
