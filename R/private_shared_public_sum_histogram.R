#' Plottet grouped barplot mit 4 Balken je Sample.
#'
#' Plottet grouped barplot mit 4 Balken je Sample.
#' private der x haeufigsten'
#' Erster Balken sind die Private(d.h. die die nicht in der Probe vorkommen die in partner.vec angegeben ist)
#' Clonotypen. Bsp. Von M1TCZ1 ausgehend werden die ersten 30 (hauefigsten) Clonotypen betrachtet (im Folgenden
#' M1TCZ1_30 genannt).  Nun wird geschaut welche Listenelement als Partner in partner.vec angegeben wurde. Sei dies
#' M1TCZ2.  Nun werden die Clonotypen gesucht welche in M1TCZ1_30 vorkommen aber nicht in M1TCZ2.  Die Summe der
#' Haeufigkeiten dieser Clonotypen ist nun der ersten Balken.
#' haeufigsten in T2 und nur in T2'
#' Der zweite Balken gibt die Summe der Haufigkeiten der Clonotypen aus M1TCZ1_30 an die in TCZ2 vorkommen, aber
#' zugleich nicht Teil der Public-Clonotypen(die Schnittmenge der Gesamtliste) sind.
#' public_also auch in allen anderen TCZ'
#' Der dritte Balken gibt die Summe der Haufigkeiten der Clonotypen aus M1TCZ1_30 an die zugleich in den
#' Public-Clonotypen(die Schnittmenge der Gesamtliste) sind.  Der vierte Balken gibt die Summe der Haeufigkeiten von
#' M1TCZ1_30 an.  Merke: Der Mengen der drei obigen Beschreibungen sind paarweise disjunkt.  PARAMETER: range gibt
#' an welche Clonotypen aus der jeweiligen Probe als Basis genommen werden sollen. z.B. 1:30 nimmt die ersten 30
#' Clonotypen jedes Samples. filename ist der Name für die CSV-Datei in der die Statistiken abgelegt werden partner
#' vec ist der Partner zu jedem Listenelement, d.h. length(l) = length(partner.vec) ret falls true wird der
#' data.frame mit Daten zurückgeben und nicht geplottet norm falls true werden die oben beschrieben Balken auf die
#' Summe der Haeufigkeiten der ausgewaehlten Clonotypen genormt
#'
#' @param l A list of data.frames
#' @param .colours Definiert die Farben fuer die Samples
#' @param filename Writes the plot data to file named by this filename.
#' @param partner.vec Plot use pairs of samples. Define the partner for each list element.
#' @param ret TRUE = return the hist.data.df, FALSE = return the plot object
#' @param norm Norm the plot values to 1.
#'
#' @examples
#' private_shared_public_sum_histogram(l.test[-c(3,6,9)], range = 1:20, partner.vec = c(2, 1, 4, 3, 6, 5), ret = F, norm = F)
#' @export
#' @import ggplot2

private_shared_public_sum_histogram <- function(l, range = 1:30, filename = NULL, 
    partner.vec = c(2, 1, 4, 3, 6, 5), ret = F, norm = F) {
    
    public.l <- Reduce(intersect, lapply(l, function(x) {
        x[, 4]
    }))
    
    count.private <- NULL
    sum.count.range <- NULL
    sum.count.private <- NULL
    sum.count.shared <- NULL
    sum.count.public <- NULL
    sum.range <- NULL
    sum.private <- NULL
    sum.shared <- NULL
    sum.public <- NULL
    for (i in seq_along(l)) {
        sum.count.range[i] <- sum(l[[i]][range, 1])
        sum.range[i] <- length(range)
        tmpUniqueAA <- uniqueAA(l[partner.vec[i]], l[[i]][range, ])[, 4]
        count.private[i] <- length(tmpUniqueAA)
        sum.count.private[i] <- sum(l[[i]][(l[[i]][, 4] %in% tmpUniqueAA), 1])
        sum.private[i] <- sum(l[[i]][, 4] %in% tmpUniqueAA)
        sum.count.public[i] <- sum(l[[i]][which((l[[i]][range, 4] %in% public.l)), 
            1])
        sum.public[i] <- sum(l[[i]][range, 4] %in% public.l)
        sum.count.shared[i] <- sum.count.range[i] - sum.count.private[i] - sum.count.public[i]
        sum.shared[i] <- sum.range[i] - sum.private[i] - sum.public[i]
        
        if (norm) {
            sum.count.private[i] <- sum.count.private[i]/sum.count.range[i]
            sum.count.shared[i] <- sum.count.shared[i]/sum.count.range[i]
            sum.count.public[i] <- sum.count.public[i]/sum.count.range[i]
            sum.count.range[i] <- 1
        }
    }
    
    # write to file
    m.data <- matrix(0, ncol = 6, nrow = length(l))
    rownames(m.data) <- names(l)
    colnames(m.data) <- c(paste0("seq counts private der ", length(range), " haeufigsten"), 
        paste0("number private der ", length(range), " haeufigsten"), paste0("seq counts ", 
            length(range), " haeufigsten in T2 und nur in T2"), paste0("number ", 
            length(range), " haeufigsten in T2 und nur in T2"), paste0("seq counts public_also auch in allen anderen TCZ"), 
        paste0("number public_also auch in allen anderen TCZ"))
    
    m.data[, 1] <- sum.count.private
    m.data[, 2] <- sum.private
    m.data[, 3] <- sum.count.shared
    m.data[, 4] <- sum.shared
    m.data[, 5] <- sum.count.public
    m.data[, 6] <- sum.public
    
    # m.data[,'sum counts'] <- sum.count.range m.data[,'sum'] <- sum.range
    
    if (!is.null(filename)) {
        write.table(m.data, file = filename, sep = ";", row.names = T, col.names = NA)
    }
    
    # long vector last col
    value.vec <- c(sum.count.range, sum.count.private, sum.count.shared, sum.count.public)
    variable.vec <- factor(c(rep("sum", length(l)), rep("private", length(l)), rep("shared", 
        length(l)), rep("public", length(l))))
    # levels(variable.vec) <- c('sum','private','public') positions <-
    # c('sum','private','public', 'shared')
    names.vec <- rep(names(l), 4)
    
    
    hist.data.df <- data.frame(names = names.vec, variable = variable.vec, value = value.vec, 
        stringsAsFactors = F)
    
    hist.data.df$names <- factor(hist.data.df$names, levels = hist.data.df$names)
    
    if (ret) 
        return(hist.data.df)
    
    plot.histo(hist.data.df, colnames(m.data))
}

plot.histo <- function(hist.data.df, count = 30, labels = c(paste0("seq counts private der ", 
    count, " haeufigsten"), paste0("number private der ", count, " haeufigsten"), 
    paste0("seq counts ", count, " haeufigsten in T2 und nur in T2"), paste0("number ", 
        count, " haeufigsten in T2 und nur in T2"), paste0("seq counts public_also auch in allen anderen TCZ"), 
    paste0("number public_also auch in allen anderen TCZ"))) {
    cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", 
        "#D55E00", "#CC79A7")
    shadesOfGrey <- colorRampPalette(c("grey0", "grey80"))
    cbPalette <- shadesOfGrey(4)
    ggplot(hist.data.df, aes(names, value)) + geom_bar(aes(fill = variable), position = "dodge", 
        stat = "identity") + scale_fill_manual(values = cbPalette, labels = c(labels[1], 
        labels[3], labels[5], "seq counts sum")) + # guide = guide_legend(direction = 'horizontal', title.position = 'top',
    # label.position='bottom', label.hjust = 0.5, label.vjust = 0.5, label.theme =
    # element_text(angle = 90))
    theme(legend.direction = "vertical", legend.box = "horizontal", legend.position = c(0, 
        1), legend.justification = c(0, 1)) + guides(fill = guide_legend(title = NULL))
}

# Plottet zwei Listen in einen Plot Achtung partner.vec muss ggf. angepasst
# werden.
plot.together <- function(l1, l2, range = 1:30, filename = "data.csv", partner.vec = c(2, 
    1, 4, 3, 6, 5), norm = F) {
    hist.data.l1 <- histogram(l1, range, filename, partner.vec, ret = T, norm)
    hist.data.l2 <- histogram(l2, range, filename, partner.vec, ret = T, norm)
    
    hist.data <- rbind(hist.data.l1, hist.data.l2)
    
    plot.histo(hist.data, count = length(range))
}

uniqueAA <- function(l, df) {
    for (n in names(l)) {
        df <- df[!(df[, 4] %in% l[[n]][, 4]), ]
    }
    return(df)
}
