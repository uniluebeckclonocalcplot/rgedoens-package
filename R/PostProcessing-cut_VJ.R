#' Cut values after first ',' in V/J.segments column
#'
#' Use \code{lapply(l, pp_cut_vj)} to apply the function to each data.frame in a list.
#'
#' @param dataframe A data.frame
#' @examples
#' pp_cut_vj(dataFrame)
#' @export

pp_cut_vj <- function(l) {
    lapply(l, function(x) {
        x[, "V.segments"] <- gsub(",.*$", "", x[, "V.segments"])
        x[, "J.segments"] <- gsub(",.*$", "", x[, "J.segments"])
        return(x)
    })
}
