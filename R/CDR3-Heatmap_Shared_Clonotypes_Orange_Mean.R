#' Plot a heatmap based on sample subsampling
#'
#' This function use always subsampling! Use the other heatmap function for non-subsampling plots.
#'
#' @param marginBottom integer() Set margin on the bottom.
#' @param marginSide integer() Set the margin on the side.
#' @param title string() Set the title of the plot
#' @param width integer() Set the width of the diagram.
#' @param height integer() Set the height of the diagram.
#' @param fontColorMap string() Set the font Color for text in the map
#' @param fontSizeMap integer() Set the font size for the text in the map
#' @param fontSizeSide integer() Set the font size of the side labels
#' @param fontSizeBottom integer() Set the font size of the bottom labels
#' @examples
#' heatmap_shared_clonotypes_orange_mean(l.test[c(3,6,9)], subsampleCount = 100)
#' @export
#' @importFrom gplots heatmap.2

heatmap_shared_clonotypes_orange_mean <- function(dataFrameList, subsampleCount = NULL, 
    runs = 100, marginBottom = 13, marginSide = 13, fontSizeBottom = 2, fontSizeSide = 2, 
    fontSizeMap = 2, fontColorMap = "black", height = 10, width = 12, title = "Heatmap shared cl ") {
    l <- dataFrameList
    
    title <- paste0(title, runs)
    
    if (height == 10) 
        height <- length(dataFrameList) * 2
    if (width == 12) 
        width <- length(dataFrameList) * 2 + 2
    
    
    # set number of random choosed clonotypes
    if (is.null(subsampleCount)) 
        subsampleCount <- min(sapply(dataFrameList, nrow))
    # make res.meanults comparable
    set.seed(42)
    
    m <- matrix(rep(list(rep(0, runs)), length(l)^2), nrow = length(l), ncol = length(l))
    rownames(m) <- colnames(m) <- names(l)
    
    # subsampling process
    for (i in 1:runs) {
        l_subsampled <- lapply(l, function(x) {
            x[sort(sample(1:nrow(x), subsampleCount)), ]
        })
        
        for (n1 in seq_along(l_subsampled)) {
            for (n2 in seq_along(l_subsampled)) {
                m[n1, n2][[1]][i] <- sum(l_subsampled[[n1]][, "CDR3.amino.acid.sequence"] %in% 
                  l_subsampled[[n2]][, "CDR3.amino.acid.sequence"])
            }
        }
    }
    
    res.mean <- matrix(0, nrow = length(l), ncol = length(l))
    rownames(res.mean) <- colnames(res.mean) <- names(l)
    res.mean.labels <- res.mean
    
    
    # calc mean
    for (n1 in seq_along(l_subsampled)) {
        for (n2 in seq_along(l_subsampled)) {
            res.mean[n1, n2] <- round(mean(m[n1, n2][[1]]))
            res.mean.labels[n1, n2] <- paste0(res.mean[n1, n2], " (", round(sd(m[n1, 
                n2][[1]]), 2), ")")
        }
    }
    
    # Plot a heatmap of the number of shared clonotypes in orange.
    # l.rownames<-sapply(l,rownames)
    if (names(dev.cur()) == "null device") 
        dev.new()
    dev.control(displaylist = "enable")
    
    # set the color key range
    max.value.wo.self.intersection <- max(apply(res.mean, 1, function(x) {
        x[-which(x %in% max(x))]
    }))
    max.value <- max(apply(res.mean, 1, max))
    min.value <- min(apply(res.mean, 1, min))
    step.size <- (max.value.wo.self.intersection - min.value)/10
    range <- seq(max.value.wo.self.intersection, min.value, by = -step.size)
    
    heatmap.2(-res.mean, cellnote = res.mean.labels, trace = "none", margins = c(marginBottom, 
        marginSide), cexCol = fontSizeBottom, cexRow = fontSizeSide, notecex = fontSizeMap, 
        notecol = fontColorMap, na.color = par("bg"), key.title = title, breaks = c(-max.value, 
            -range), Colv = FALSE, Rowv = FALSE, dendrogram = "none")
    
    return(recordPlot())
}
